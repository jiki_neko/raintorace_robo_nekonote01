# Target specific macros
TARGET = nekonote16k
NXTOSEK_ROOT = /usr/local/nxtOSEK
TARGET_SOURCES = \
	nekonote16.c
TOPPERS_OSEK_OIL_SOURCE = ./nekonote16.oil

# Don't modify below part
O_PATH ?= build
include ../../ecrobot/ecrobot.mak
