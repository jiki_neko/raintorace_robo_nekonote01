/*
        _     _ 
       /^\___/^\     z
  __  |  __  __ )   z
  \ \ /=   ~~  = 
             _                     _
  _ __   ___| | _____  _ __   ___ | |_  ___
 | '_ \ / _ \ |/ / _ \| '_ \ / _ \| __|/ _ \ 
 | | | |  __/   < (_) | | | | (_) | |_|  __/
 |_| |_|\___|_|\_\___/|_| |_|\___/\___|\___|
 nxt_raintrace_nekonote:16.0
 update:2015/12/20
 Author:hiro
*/
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include "stdlib.h"

#define SONAR_SENSOR NXT_PORT_S4
#define COLOR_SENSOR NXT_PORT_S3
#define L_MOTOR NXT_PORT_B
#define R_MOTOR NXT_PORT_C
#define L_LIGHT NXT_PORT_S1
#define R_LIGHT NXT_PORT_S2

//基本設定　(ここの数値の設定を優先的にすすめる)
#define BLACK_VAL 660  //白と同じ値づつ調整する感じになるはず
#define WHITE_VAL 430
#define GRAY_VAL 520  //(BLACK_VAL + WHITE_VAL) / 2 から -20 くらいの設定をする
#define BASE_SPEED 41 //全体のスピードを上げる場合はここの数値を上げる(とりあえず40くらいが一番安定する)
#define P_GAIN 0.41  //0.01単位で調整する(大きくすると黒色に反応しやすくなる)()
#define D_GAIN 10.0

#define ADJUST_RIGHT_SENSOR 15  //右　ライトセンサへの調整

//90ど回転用(この２つはあまりいじらなくてもいいかも)
#define ANGLE90_1 120
#define ANGLE90_2 113//上より -10 くらいがいい

#define BALL_CATCH_DISTANCE 420//<--曲がってからボールを取るまでの距離　調整が必要

#define TURN180 255//180度回転用

//灰色ゾーン用定数
#define GRAY_ZONE_BLACK_VAL 520  //BLACK_VAL + 100 を設定
#define GRAY_ZONE_GRAY_VAL 470  //WHITE_VAL + 40 を設定
#define GRAY_ZONE_P_GAIN 0.42  //灰色への反応が悪いときは上げる

//ソナーセンサの距離
#define SONAR_SENSOR_DISTANCE 42

/* ★★★ ドラゲナイ定義  ここから↓↓ ★★★ */
#define TEMPO  25
#define VOLUME 50
#define REST   0
#define DO4 261 //C4
#define RE4 293 //D4
#define MI4 329 //E4
#define FA4 349 //F4
#define SO4 391 //G4
#define RA4 440 //A4
#define SI4 493 //B4
#define DO5 523 //C5
#define RE5 587 //D5
#define MI5 659 //E5
#define FA5 698 //F5
#define SO5 783 //G5
#define RA5 880 //A5

// 楽譜データ
int tone_dora[60][2] = {
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO*2}, {REST, TEMPO*0.5},
	{RA5,  TEMPO/2}, {SO5,  TEMPO}, {SO5,  TEMPO*2}, {REST, TEMPO*0.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO*2}, {REST, TEMPO*0.5},
	{RA4,  TEMPO/2}, {DO5,  TEMPO}, {MI5,  TEMPO*2}, {REST, TEMPO*0.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO}, {RA4,  TEMPO*1.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO}, {SO4,  TEMPO*1.5}, {REST, TEMPO*3.5},
	{SO4,  TEMPO}, {FA5,  TEMPO}, {MI5,  TEMPO}, {DO5,  TEMPO}, {RE5,  TEMPO*2.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO*2}, {REST, TEMPO*0.5},
	{RA5,  TEMPO/2}, {SO5,  TEMPO}, {SO5,  TEMPO*2}, {REST, TEMPO*0.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO*2}, {REST, TEMPO*0.5},
	{RA4,  TEMPO/2}, {DO5,  TEMPO}, {MI5,  TEMPO*2}, {REST, TEMPO*0.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO}, {RA4,  TEMPO*1.5},
	{MI5,  TEMPO/2}, {RE5,  TEMPO}, {DO5,  TEMPO}, {SO4,  TEMPO*1.5}, {REST, TEMPO*3.5},
	{SO4,  TEMPO}, {RA4,  TEMPO}, {DO5,  TEMPO}, {SI4,  TEMPO}, {DO5,  TEMPO*2.5}
};
/* ★★★ ドラゲナイ定義  ここまで↑↑ ★★★ */

DeclareCounter(SysTimerCnt);
DeclareTask(Task1);

void reset_rotate_angle();//rotate_angleなどのリセット
void find_line_by_light(int n);//pd制御からの曲がり角検知
void turn_corner(int mode,int angle);//左右回転
int end_turn_by_angle(int mode,int angle);//左右回転の終了角度
void tyousei();//曲がり角回転前の調整
void run_pid();//直線のpd制御

int color_100();
int what_is_color();

void RingTone();

static int left_speed,right_speed;
static int right_turn,left_turn;
static int state=(-1);
static int r_right,l_right;
static int l_rotate_angle,r_rotate_angle;
static S8 count=0,l;
static int online;
static int r_right_tmp=0,l_light_tmp=0;
static int n0_r_right,n0_l_right,n1_r_right,n1_l_right;
static int m;
static int n_l_light[100],n_r_light[100];
static int color=1;

S16 rgb[3];
S32 rgb_ave[3]={0,0,0};

static int soner;

void ecrobot_device_initialize(){
	ecrobot_init_bt_slave("LEJOS-OSEK");
	ecrobot_set_light_sensor_active(L_LIGHT);
	ecrobot_set_light_sensor_active(R_LIGHT);
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	nxt_motor_set_speed(NXT_PORT_A,0,0);
	ecrobot_init_nxtcolorsensor(COLOR_SENSOR, NXT_COLORSENSOR);
	ecrobot_init_sonar_sensor(SONAR_SENSOR);
	state=(-1);
}		//OSEK起動時の処理

void ecrobot_device_terminate(){
	ecrobot_set_light_sensor_inactive(L_LIGHT);
	ecrobot_set_light_sensor_inactive(R_LIGHT);
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
	ecrobot_term_bt_connection();
	nxt_motor_set_speed(NXT_PORT_A,0,0);
	ecrobot_term_sonar_sensor(SONAR_SENSOR); 
}		//OSEK終了時の処理

void user_1ms_isr_type2(void){
	StatusType ercd;

	ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */
	if (ercd != E_OK)
	{
		ShutdownOS(ercd);
	}
}

TASK(Task1)
{		
	static float x,y;
	
	static S8 i,j;
	
	static int time=0;
	static int ttiimmee=0;
	static int soner_count=0;
	
	//カラーセンサー用
	ecrobot_process_bg_nxtcolorsensor();
	ecrobot_set_nxtcolorsensor(COLOR_SENSOR,NXT_COLORSENSOR);//動作モード設定
	ecrobot_get_nxtcolorsensor_rgb(COLOR_SENSOR, rgb);
		
	
	n0_r_right = n1_r_right;
	n0_l_right = n1_l_right;

	n1_r_right = ecrobot_get_light_sensor(R_LIGHT)+ADJUST_RIGHT_SENSOR;
	n1_l_right = ecrobot_get_light_sensor(L_LIGHT);//調整とかする？
	
	
	
	r_right=(n0_r_right+n1_r_right)/2;
	l_right=(n0_l_right+n1_l_right)/2;
	
	right_turn = P_GAIN * (r_right - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) 
	     + D_GAIN * (r_right-r_right_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
		 
	left_turn = P_GAIN * (l_right - GRAY_VAL)*100 / (BLACK_VAL - WHITE_VAL) 
	     + D_GAIN * (l_right-l_light_tmp)/(float)(BLACK_VAL - WHITE_VAL )*100;
		 
	r_right_tmp=r_right;
	l_light_tmp=l_right;
	
	n_r_light[i%100]=r_right;
	n_l_light[i%100]=l_right;
	
	//線読み　左　右センサT字路検知  now=>lef tsensor next=>right sensor 
	if(state==(-1)){
		run_pid(1);
		
		if(!nxt_motor_get_count(NXT_PORT_A) > 0) nxt_motor_set_speed(NXT_PORT_A,+40,1);
		
		find_line_by_light(0);
	}
	if(state==0){
		tyousei();
	}
	//右90回転
	if(state==1){
		turn_corner(1,ANGLE90_1);
	}
	if(state==2){
		run_pid(0);
		//カラーセンサデータからたまをつかむ やめて　距離で
		if((nxt_motor_get_count(L_MOTOR) > l_rotate_angle + BALL_CATCH_DISTANCE)){
			nxt_motor_set_speed(L_MOTOR,0,0);
			nxt_motor_set_speed(R_MOTOR,0,0);
			state++;;
		}
	}
	//たまつかむ
	if(state==3){
		run_pid(0);
		if(nxt_motor_get_count(NXT_PORT_A) > 0) nxt_motor_set_speed(NXT_PORT_A,-40,1);
		else {
			reset_rotate_angle();
			state++;
		}
	}//バック
	if(state==4){
		nxt_motor_set_speed(L_MOTOR,-60,1);
		nxt_motor_set_speed(R_MOTOR,-60,1);
		if(nxt_motor_get_count(L_MOTOR) < l_rotate_angle-50){
			reset_rotate_angle();
			state++;;
		}
	}
	//180回転
	if(state==5){
		turn_corner(5,TURN180);
	}
	//pd
	if(state==6){
		run_pid(1);
		find_line_by_light(0);
	}
	if(state==7){
		tyousei();
	}
	//右90回転
	if(state==8){
		turn_corner(2,ANGLE90_2-5);
	}
	//pd
	if(state==9){
		run_pid(1);
		find_line_by_light(0);
	}
	if(state==10){
		tyousei();
	}
	//右90回転
	if(state==11){
		turn_corner(1,ANGLE90_1);
	}
	if(state==12){
		run_pid(0);
		find_line_by_light(1);
	}
	if(state==13){
		tyousei();
	}
	//左90回転
	if(state==14){
		turn_corner(4,ANGLE90_2);
	}
	if(state==15){
		run_pid(0);
		find_line_by_light(1);
	}
	if(state==16){
		tyousei();
	}
	//左90回転
	if(state==17){
		turn_corner(3,ANGLE90_1);
	}
	if(state==18){
		run_pid(1);
		find_line_by_light(0);
	}
	if(state==19){
		tyousei();
	}
	//右90回転
	if(state==20){
		turn_corner(2,ANGLE90_2);
	}
	//pd
	if(state==21){
		run_pid(1);
		find_line_by_light(0);
	}
	if(state==22){
		tyousei();
	}
	//右90回転
	if(state==23){
		turn_corner(1,ANGLE90_1);
	}
	if(state==24){
		run_pid(0);
		find_line_by_light(1);
	}
	if(state==25){
		tyousei();
	}
	//左90回転
	if(state==26){
		turn_corner(4,ANGLE90_2);
	}
	if(state==27){
		run_pid(0);
		find_line_by_light(1);
	}
	if(state==28){
		tyousei();
	}
	//左90回転
	if(state==29){
		turn_corner(4,ANGLE90_2);
	}
	if(state==30){
		run_pid(0);
		x=color_100();//色判断用準備
		find_line_by_light(1);
	}
	if(state==31){
		tyousei();
	}
	if(state==32){
		run_pid(99);
		color=what_is_color();
		state=33;
		
	}
	//左90回転
	if(state==33){
		turn_corner(4,ANGLE90_2);
	}
	 
	if(state==34){
		run_pid(0);
		if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle+400){
			reset_rotate_angle();
			state++;
		}
	}
	//gray部分
	if(state==35){
		run_pid(10);
		if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle+1000){
			reset_rotate_angle();
			state++;
		}
	}
	if(state==36){
		run_pid(0);
		find_line_by_light(1);
	}
	if(state==37){
		tyousei();
	}
	//左右90回転どっちっち
	if(state==38){
		if(color==0){
			turn_corner(4,ANGLE90_2+5);
		}else if(color==1||color==2){
			turn_corner(1,ANGLE90_1-10);
		}
	}
	//ボールつかむまでの直線
	if(state==39){
		if(color==0){
			run_pid(1);
			if(nxt_motor_get_count(L_MOTOR) > l_rotate_angle+200){
				reset_rotate_angle();
				state++;
			}
		}else if(color==1||color==2){
			run_pid(0);
			if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle+200){
				reset_rotate_angle();
				state++;
			}
		}
	}
	//ボールつかむまでの直線
	if(state==40){	
		nxt_motor_set_speed(NXT_PORT_A,+40,1);
		if(color==0){
			run_pid(1);
			
			if(nxt_motor_get_count(L_MOTOR) > l_rotate_angle+80){
				reset_rotate_angle();
				state++;
			}
		}else if(color==1||color==2){
			run_pid(0);
			if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle+80){
				reset_rotate_angle();
				state++;
			}
		}
	}
	//ちょっとだけバック
	if(state==41){
        nxt_motor_set_speed(NXT_PORT_A,-30,1);
		nxt_motor_set_speed(L_MOTOR,-40,1);
		nxt_motor_set_speed(R_MOTOR,-40,1);
		if(nxt_motor_get_count(R_MOTOR) < r_rotate_angle-10){
			reset_rotate_angle();
			state++;
		}
	}
	//黒線を目標に回転
	if(state==42){
		if(color==0){
			nxt_motor_set_speed(L_MOTOR,2,1);
			nxt_motor_set_speed(R_MOTOR,-40,1);
			if(r_right>GRAY_VAL+20){
				reset_rotate_angle();
				state++;
			}
		}else if(color==1||color==2){
			nxt_motor_set_speed(L_MOTOR,-40,1);
			nxt_motor_set_speed(R_MOTOR,2,1);
			if(l_right>GRAY_VAL+20){
				reset_rotate_angle();
				state++;
			}
		}
	}
	//かべを見つける
	if(state==43){
		if(ttiimmee<150){
			if(color==0){
				nxt_motor_set_speed(L_MOTOR,45,1);
				nxt_motor_set_speed(R_MOTOR,30,1);
			}else if(color==1||color==2){
				nxt_motor_set_speed(L_MOTOR,30,1);
				nxt_motor_set_speed(R_MOTOR,45,1);
			}
		}else{
			if(color==0){
				nxt_motor_set_speed(L_MOTOR,28,1);
				nxt_motor_set_speed(R_MOTOR,40,1);
			}else if(color==1||color==2){
				nxt_motor_set_speed(L_MOTOR,40,1);
				nxt_motor_set_speed(R_MOTOR,28,1);
			}
		}
		if(time==24){
			//50ms以上の間隔で実行する必要あり<-sonor
			soner=ecrobot_get_sonar_sensor(SONAR_SENSOR);
			time=0;
		}
		if(ttiimmee==900){
			
			ttiimmee=0;
		}
		if(soner<SONAR_SENSOR_DISTANCE && soner!=(-1) && time==0){
			soner_count+=1;
			if(soner_count>5){
				state++;
			}
		}
		time++;
		ttiimmee++;
	}
	if(state==44){
		run_pid(99);
		state++;
	}
	if(state==45){
		run_pid(99);
		RingTone(); // ★★★演奏する
	}
	
	
	// display_goto_xy(0, 2);
	// display_string("R:");
	// display_int(rgb[0],0);
	// display_goto_xy(0, 3);
	// display_string("G:");
	// display_int(rgb[1],0);
	// display_goto_xy(0, 4);
	// display_string("B:");
	// display_int(rgb[2],0);
	// display_update();
	
	
	display_goto_xy(0, 1);
	display_string("state=");
	display_int(state,0);
	display_goto_xy(0, 2);
	display_string("online=");
	display_int(online,0);
	display_goto_xy(0, 3);
	display_string("time=");
	display_int(time,0);
	display_goto_xy(0, 4);
	display_string("rgb_ave[0]=");
	display_int(rgb_ave[0],0);
	display_goto_xy(0, 5);
	display_string("r_light=");
	display_int(r_right,0);
	display_goto_xy(0, 6);
	display_string("l_light=");
	display_int(l_right,0);
	display_goto_xy(0, 7);
	display_string("soner=");
	display_int(soner,0);

	display_update();

	
	ecrobot_bt_data_logger(count, online);//ログ収集
	if(i==100)i=0;

	TerminateTask();
}

void reset_rotate_angle(){
	l_rotate_angle=nxt_motor_get_count(L_MOTOR);
	r_rotate_angle=nxt_motor_get_count(R_MOTOR);
	count=0;
	l=0;
	online=0;
	right_turn=0;
	left_turn=0;
	nxt_motor_set_speed(L_MOTOR,0,0);
	nxt_motor_set_speed(R_MOTOR,0,0);
}
void run_pid(int mode){
	//右でよむ---------------------------------------------------------------
	
	if(mode==0){
		if(n_r_light[m]>GRAY_VAL){
			if(count<50){
				count+=6;
			}else count=50;
		}else{
			if(count>-50){
				count--;
			}else count=-50;
		}
		if(online==2 && count>0){
			online=2;
		}else if(count>0 && nxt_motor_get_count(R_MOTOR) > r_rotate_angle+150){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>30){
				online=2;
				l=0;
			}
		}else{
			l=0;	
		}
		
		
		left_speed=BASE_SPEED+right_turn;
		right_speed=BASE_SPEED-right_turn;
		
		if(online==0||online==1){
			left_speed+=(right_turn)/2;
			right_speed-=(right_turn)/2;
			left_speed+=16;
			right_speed+=16;
		}
		
		if(online==2 && state!=34 && state!=39){
			left_speed+=40;
			right_speed+=40;
			l=0;
		}
        if(state==39){
			left_speed-=10;
			right_speed-=10;
			l=0;
		}
		if(state==40){
			left_speed+=30;
			right_speed+=30;
		}
		
		if(nxt_motor_get_count(R_MOTOR) < r_rotate_angle+200){
			left_speed-=30;
			right_speed-=30;
			left_speed-=(right_turn)/2;
			right_speed+=(right_turn)/2;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	
	//左で読む---------------------------------------------------------------
	
	if(mode==1){
		if(n_l_light[m]>GRAY_VAL){
			if(count<80){
				count+=5;
			}else count=50;
		}else{
			if(count>-80){
				count--;
			}else count=-50;
		}
		
		if(online==2 && count>0){
			online=2;
		}else if(count>0 && nxt_motor_get_count(L_MOTOR) > l_rotate_angle+150){
			online=1;
		}else online=0;
	
		if(online==1){
			l++;
			if(l>40){
				online=2;
				l=0;
			}
		}else{
			l=0;
		}
		
	
		left_speed=BASE_SPEED-left_turn;
		right_speed=BASE_SPEED+left_turn;
		
		if(online==0||online==1){
			left_speed-=(left_turn)/2;
			right_speed+=(left_turn)/2;
			left_speed+=16;
			right_speed+=16;
		}
		
		
		if(online==2 && state!=2 && state!=39){
			left_speed+=40;
			right_speed+=40;
			l=0;
		}
        if(state==39){
			left_speed-=10;
			right_speed-=10;
			l=0;
		}
		if(state==40){
			left_speed+=30;
			right_speed+=30;
		}
		
		if(nxt_motor_get_count(L_MOTOR) < l_rotate_angle+200){
			left_speed-=30;
			right_speed-=30;
			left_speed+=(left_turn)/2;
			right_speed-=(left_turn)/2;
		}
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	
	
	//-------------------------------------------------------------
	//右でよむXXX---------------------------------------------------------------
	
	if(mode==10){
		if(r_right > GRAY_ZONE_BLACK_VAL) r_right=GRAY_ZONE_BLACK_VAL;
		if(l_right > GRAY_ZONE_BLACK_VAL) l_right=GRAY_ZONE_BLACK_VAL;
		
		right_turn = GRAY_ZONE_P_GAIN * (r_right - (GRAY_ZONE_GRAY_VAL))*100 / (GRAY_ZONE_BLACK_VAL - WHITE_VAL) ;
	     //+ D_GAIN * (r_right-r_right_tmp)/(float)((GRAY_ZONE_BLACK_VAL+50) - WHITE_VAL )*100;
		 
		left_turn = GRAY_ZONE_P_GAIN * (l_right - (GRAY_ZONE_GRAY_VAL))*100 / (GRAY_ZONE_BLACK_VAL - WHITE_VAL) ;
	     //+ D_GAIN * (l_right-l_light_tmp)/(float)((GRAY_ZONE_BLACK_VAL+50) - WHITE_VAL )*100;
				
		online=0;
		
		left_speed=BASE_SPEED+right_turn;
		right_speed=BASE_SPEED-right_turn;
        
        left_speed-=5;
		right_speed-=5;
        
        // left_speed+=(right_turn)/2;
        // right_speed-=(right_turn)/2;
		
		if(left_speed>35) left_speed=35;
		if(right_turn>35) right_turn=35;
		if(left_speed<10) left_speed=10;
		if(right_turn<10) right_turn=10;
		
		nxt_motor_set_speed(L_MOTOR,left_speed,1);
		nxt_motor_set_speed(R_MOTOR,right_speed,1);
	}
	
	//-------------------------------------------------------------
	if(mode==99){
		nxt_motor_set_speed(L_MOTOR,0,0);
		nxt_motor_set_speed(R_MOTOR,0,0);
	}
}

//引数 0:right 1:left
void find_line_by_light(int n){
	if(n==0){
		if(r_right>GRAY_VAL+20 
		&& nxt_motor_get_count(L_MOTOR) > l_rotate_angle+300){
			reset_rotate_angle();
			state+=1;
		}
	}else if(n==1){
		if(l_right>GRAY_VAL+20 
		&& nxt_motor_get_count(R_MOTOR) > r_rotate_angle+300){
			reset_rotate_angle();
			state+=1;
		}
	}
}
void tyousei(){
	if(online==0||online==1){
		nxt_motor_set_speed(L_MOTOR,30,1);
		nxt_motor_set_speed(R_MOTOR,30,1);
		if(nxt_motor_get_count(L_MOTOR)>l_rotate_angle+25){
			state+=1;
			reset_rotate_angle();
		}
	}else if (online==2){
		nxt_motor_set_speed(L_MOTOR,-40,1);
		nxt_motor_set_speed(R_MOTOR,-40,1);
		  if(nxt_motor_get_count(L_MOTOR)<l_rotate_angle-5){
			state+=1;
			reset_rotate_angle();
		  }
	}
}

void turn_corner(int mode,int angle){
	static int status=0;
	switch(mode){
		//右回転次のセンサ違うとき
		case 1:
			nxt_motor_set_speed(L_MOTOR,40,1);
			nxt_motor_set_speed(R_MOTOR,-40,1);
			status=end_turn_by_angle(mode,angle);
			break;
		//右回転次のセンサ同じとき
		case 2:
			if(status==0){
				nxt_motor_set_speed(L_MOTOR,-20,1);
				nxt_motor_set_speed(R_MOTOR,-20,1);
				if(nxt_motor_get_count(L_MOTOR) < l_rotate_angle-7){
					status=1;
				}
			}else{
				nxt_motor_set_speed(L_MOTOR,40,1);
				nxt_motor_set_speed(R_MOTOR,-35,1);
				status=end_turn_by_angle(mode,angle);
			}
			break;
		//左回転次のセンサ違うとき
		case 3:
			nxt_motor_set_speed(L_MOTOR,-40,1);
			nxt_motor_set_speed(R_MOTOR,40,1);
			status=end_turn_by_angle(mode,angle);
			break;
		//左回転次のセンサ同じとき
		case 4:
			if(status==0){
				nxt_motor_set_speed(L_MOTOR,-20,1);
				nxt_motor_set_speed(R_MOTOR,-20,1);
				if(nxt_motor_get_count(R_MOTOR) < r_rotate_angle-7){
					status=1;
				}
			}else{
				nxt_motor_set_speed(L_MOTOR,-35,1);
				nxt_motor_set_speed(R_MOTOR,40,1);
				status=end_turn_by_angle(mode,angle);
			}
			break;
		//180回転
		case 5:
			nxt_motor_set_speed(L_MOTOR,55,1);
			nxt_motor_set_speed(R_MOTOR,-40,1);
			// if(nxt_motor_get_count(L_MOTOR) > l_rotate_angle+110 && nxt_motor_get_count(L_MOTOR) < l_rotate_angle+150){
			// 	nxt_motor_set_speed(L_MOTOR,40,1);
			// 	nxt_motor_set_speed(R_MOTOR,40,1);
			// }
			status=end_turn_by_angle(mode,angle);
			break;
		case 6:
			nxt_motor_set_speed(L_MOTOR,-30,1);
			nxt_motor_set_speed(R_MOTOR,30,1);
			// if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle+110 && nxt_motor_get_count(R_MOTOR) < r_rotate_angle+150){
			// 	nxt_motor_set_speed(L_MOTOR,40,1);
			// 	nxt_motor_set_speed(R_MOTOR,40,1);
			// }
			status=end_turn_by_angle(mode,angle);
			break;
	}
}
 int end_turn_by_angle(int mode,int angle){
	switch(mode){
		//右回転
		case 1:
		case 2:
		case 5:
			if(nxt_motor_get_count(L_MOTOR) > l_rotate_angle + angle){
				reset_rotate_angle();
				state+=1;
				return 0;
			}
			break;
		//左回転
		case 3:
		case 4:
		case 6:
			if(nxt_motor_get_count(R_MOTOR) > r_rotate_angle + angle){
				reset_rotate_angle();
				state+=1;
				return 0;
			}
			break;
	}
}

//rgbの100個分のデータをrgb_aveに入れる
int color_100(){
	static int nnn=1;
	if(nnn<100){
		rgb_ave[0]+=rgb[0];
		rgb_ave[1]+=rgb[1];
		rgb_ave[2]+=rgb[2];
		nnn++;
	}
	return 0;
}

//色の判別
int what_is_color(){
	int n;
	int max,max_n;
	int color2[3];
	int ave_3color;
	int bunsan;
	
	//大きめの数字で割ることで情報量を減らす
	color2[0]=rgb_ave[0]/1000;
	color2[1]=rgb_ave[1]/1000;
	color2[2]=rgb_ave[2]/1000;
	
	//分散値の計算
	ave_3color=(color2[0]+color2[1]+color2[2])/3;
	bunsan=(((color2[0]-ave_3color)*(color2[0]-ave_3color)) + ((color2[1]-ave_3color)*(color2[1]-ave_3color)) + ((color2[2]-ave_3color)*(color2[2]-ave_3color)))/3;
	
	// display_goto_xy(0, 6);
	// display_string("bunsan=");
	// display_int(bunsan,0);
	// display_goto_xy(0, 7);
	// display_string("ave_3color=");
	// display_int(ave_3color,0);
	
	//最も値が大きかったものをそこにある色として、max_nに格納
	//red:0 green:1 blue:2
	max=rgb_ave[0];
	max_n=0;
	for(n=1;n<3;n++){
		if(max<rgb_ave[n]){
			max=rgb_ave[n];
			max_n=n;
		}
	}
	return max_n;	
}
/* ★★★ 楽譜演奏関数  ここから↓↓ ★★★ */
void RingTone(){	/* 音符を再生するユーザー関数を定義 */
	int i;
	
	/* 楽譜データ数分ループ */
	for(i=0; i<60; i++) {
		if(tone_dora[i][0] == REST) {
			/* 休符の場合 */
			systick_wait_ms(tone_dora[i][1]);
		}
		else {
			/* 音符の場合 */
			ecrobot_sound_tone(tone_dora[i][0], tone_dora[i][1]*5, VOLUME);
			systick_wait_ms(tone_dora[i][1]*10);
		}
	}
}
/* ★★★ 楽譜演奏関数  ここまで↑↑ ★★★ */

